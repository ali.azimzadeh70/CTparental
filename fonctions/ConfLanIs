i_WAN_ipv4="$(ip -4 route | grep default | awk '{print $5}')" # GW!
if [ "$i_WAN_ipv4" =  "" ];then
    ipbox_ipv4=""
    ip_i_WAN_ipv4=""
    ip_broadcast_ipv4=""
    reseau_box_ipv4=""
else
    ipbox_ipv4="$(ip -4 route | grep default | awk '{print $3}')"
    ip_i_WAN_ipv4="$(ip -4 address show "$i_WAN_ipv4" | awk '/inet /{print substr($2,1)}')"
    ip_broadcast_ipv4="$(ip -4 address show "$i_WAN_ipv4" | awk '/brd /{print substr($4,1)}')"
    reseau_box_ipv4="$(ip -4 route | grep "$i_WAN_ipv4" | grep -v default | grep "src" | awk '//{print substr($1,1)}' )"
fi

i_WAN_ipv6="$(ip -6 route | grep default | awk '{print $5}')" # GW!
if [ "$i_WAN_ipv6" =  "" ];then
    ipbox_ipv6=""
    ip_i_WAN_ipv6=""
    reseau_box_ipv6_g=""
else
    ipbox_ipv6="$(ip -6 route | grep default | awk '{print $3}')"
    ip_i_WAN_ipv6="$(ip -6 address show "$i_WAN_ipv6" | awk '/inet/{print substr($2,1)}')"
    reseau_box_ipv6_g="$(ip -6 route | grep "$i_WAN_ipv6" | grep -v default | awk '//{print substr($1,1)}')"
fi

## détection DNS via nmcli
nmcli dev show > /dev/null 2>&1
if [ $? -eq 0 ];then
    if [ ! "$i_WAN_ipv4" = "" ] ;then
        dnsv4=$(nmcli dev show "$i_WAN_ipv4" | grep "IP4.DNS" | awk '{print $2}')
        DNS1v4="$(echo $dnsv4 | cut -d " " -f1)"
        DNS2v4="$(echo $dnsv4 | cut -d " " -f2)"
    fi
    if [ ! "$i_WAN_ipv6" = "" ] ;then
        dnsv6="$(nmcli dev show "$i_WAN_ipv6" | grep "IP6.DNS" | awk '{print $2}' )"
        DNS1v6="$(echo $dnsv6 | cut -d " " -f1)"
        DNS2v6="$(echo $dnsv6 | cut -d " " -f2)"
    fi
fi

## détection DNS via systemd-resolve si mncli na pas fonctioné
systemd-resolve --status > /dev/null 2>&1
if [ $? -eq 0 ];then
    if [ "$DNS1v4" = "" ] ;then
        if [ ! "$i_WAN_ipv4" = "" ] ;then
            dnsv4="$(systemd-resolve --status -i $i_WAN_ipv4 | grep -A4 'DNS Servers'| grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )"
            DNS1v4="$(echo $dnsv4 | cut -d " " -f1)"
            DNS2v4="$(echo $dnsv4 | cut -d " " -f2)"
        fi
    fi
    if [ "$DNS1v6" = "" ] ;then
        if [ ! "$i_WAN_ipv6" = "" ] ;then
            dnsv6="$(systemd-resolve --status -i $i_WAN_ipv6 | grep -A4 'DNS Servers' | grep -E "^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(([0-9A-Fa-f]{1,4}:){0,5}:((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(::([0-9A-Fa-f]{1,4}:){0,5}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$" )"
            DNS1v6="$(echo $dnsv6 | cut -d " " -f1)"
            DNS2v6="$(echo $dnsv6 | cut -d " " -f2)"
        fi
    fi
fi

## détection DNS via /etc/resolv.conf
if [ "$DNS1v4" = "" ] ;then
    dnsv4="$(grep nameserver /etc/resolv.conf | awk '{print $2}' | grep -v "^127." | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")"
    DNS1v4="$(echo $dnsv4 | cut -d " " -f1)"
    DNS2v4="$(echo $dnsv4 | cut -d " " -f2)"
fi
if [ "$DNS1v6" = "" ] ;then
    dnsv6="$(grep nameserver /etc/resolv.conf | awk '{print $2}' | grep -v "^::1" | grep -E "^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(([0-9A-Fa-f]{1,4}:){0,5}:((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|(::([0-9A-Fa-f]{1,4}:){0,5}((b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b).){3}(b((25[0-5])|(1d{2})|(2[0-4]d)|(d{1,2}))b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$")"
    DNS1v6="$(echo $dnsv6 | cut -d " " -f1)"
    DNS2v6="$(echo $dnsv6 | cut -d " " -f2)"
fi

## détection DNS via dig les autres métodes ont échouées
if [ "$DNS1v4" = "" ] ;then
    handelsrule=$(nft list table filter -a | grep "skuid \"ctdigtest\"" | cut -d "#" -f2 | cut -d " " -f3)
    for i in $handelsrule
    do
      nft delete rule filter out handle $i
    done
    DNS1v4="$(dig -4 +all +retry=1 +tries=1 | grep SERVER | cut -d "(" -f2 | sed -e "s/)//g")"
    if [ ! "$DNS1v4" =  "" ];then
       nft add rule ip filter out ip daddr "$DNS1v4" skuid ctdigtest udp dport 53 reject
       handelsrule=$(nft list table filter -a | grep "skuid \"ctdigtest\"" | cut -d "#" -f2 | cut -d " " -f3)
       DNS2v4="$(sudo -u ctdigtest dig -4 +all +retry=2 +tries=1 | grep SERVER | cut -d "(" -f2 | sed -e "s/)//g")"
       nft delete rule filter out handle $handelsrule
    fi
fi
if [ "$DNS1v6" = "" ] ;then
    handelsrule=$(nft list table ip6 filter -a | grep "skuid \"ctdigtest\"" | cut -d "#" -f2 | cut -d " " -f3)
    for i in $handelsrule
    do
      nft delete rule ip6 filter out handle $i
    done
    DNS1v6="$(dig -6 +all +retry=1 +tries=1 | grep SERVER | cut -d "(" -f2 | sed -e "s/)//g")"
    if [ ! "$DNS1v6" =  "" ];then
       nft add rule ip6 filter out ip6 daddr "$DNS1v6" skuid ctdigtest udp dport 53 reject
       handelsrule=$(nft list table ip6 filter -a | grep "skuid \"ctdigtest\"" | cut -d "#" -f2 | cut -d " " -f3)
       DNS2v6="$(sudo -u ctdigtest dig -6 +all +retry=2 +tries=1 | grep SERVER | cut -d "(" -f2 | sed -e "s/)//g")"
       nft delete rule ip6 filter out handle $handelsrule
    fi
fi

## exportation des varriables
export i_WAN_ipv4
export ipbox_ipv4
export ip_i_WAN_ipv4
export reseau_box_ipv4
export ip_broadcast_ipv4
export DNS1v4=${DNS1v4:=8.8.8.8}
export DNS2v4=${DNS2v4:=8.8.4.4}

reseau_box_ipv6_l="fe80::/64"

export i_WAN_ipv6
export ipbox_ipv6
export ip_i_WAN_ipv6
export reseau_box_ipv6_g
export reseau_box_ipv6_l
export DNS1v6=${DNS1v6:=2001:4860:4860::8888}
export DNS2v6=${DNS2v6:=2001:4860:4860::8844}
